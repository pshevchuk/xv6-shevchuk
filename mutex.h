#ifndef MUTEX_H
#define MUTEX_H
#include "spinlock.h"

struct mutex {
  struct spinlock lock;
  struct proc *owner;
  struct inode *ip;
  int ref;
};

void mutexinit(void);
struct mutex *mutexalloc(void);
struct mutex *mutexdup(struct mutex *mtx);
int mutexclose(struct mutex *mtx);
int mutextrylock(struct mutex *mtx);
int mutexlock(struct mutex *mtx);
int mutexunlock(struct mutex *mtx);
int mdalloc(struct mutex *mtx);

#endif
