#include "parse_passwd.h"
#include "types.h"
#include "user.h"
#include "fcntl.h"
#include "stat.h"

static char *
tok(char **data, char *end, char delim) {
  if (*data == 0 || *data == end)
    return 0;
  char *result = *data;
  for (; *data < end; ++*data) {
    if (**data == delim) {
      **data = 0;
      ++*data;
      return result;
    }
  }
  **data = 0;
  return result;
}

static int
get_id(char *d) {
  int result = 0;
  for (; *d; ++d) {
    if (*d < '0' || '9' < *d ||
        result > 214748364 || (result == 214748364 && *d > '7'))
      return -1;
    result = result * 10 + (*d -'0');
  }
  return result;
}

static void
free_list(struct passwd_entry *head) {
  if (!head)
    return;
  struct passwd_entry *cur = head->next;
  while (cur) {
    free(head);
    head = cur;
    cur = cur->next;
  }
  free(head);
}

static inline char *
check_empty(char *x) {
  if (*x)
    return x;
  else
    return 0;
}

struct passwd_entry *
read_passwd_file(const char *passwd_path) {
  int fd = open(passwd_path, O_RDONLY);
  if (fd < 0) {
    return (struct passwd_entry *)-1;
  }
  struct stat st;
  fstat(fd, &st);
  char *data = malloc(st.size + 1);
  read(fd, data, st.size);
  close(fd);
  data[st.size] = 0;
  struct passwd_entry *result = 0;
  char *line;
  char *cur_data = data;
  while ((line = tok(&cur_data, data + st.size, '\n')) != 0) {
    char *line_end = line;
    while (*line_end)
      ++line_end;
    char *d[7];
    for (int i = 0; i < 7; ++i) {
      d[i] = tok(&line, line_end, ':');
    }
    if (tok(&line, line_end, ':') != 0 || d[6] == 0) {
      free_list(result);
      free(data);
      return (struct passwd_entry *)-1;
    }
    struct passwd_entry *newHead = malloc(sizeof(struct passwd_entry));
    newHead->next = result;
    result = newHead;
    newHead->username = d[0];
    newHead->password = check_empty(d[1]);
    newHead->uid = get_id(d[2]);
    newHead->gid = get_id(d[3]);
    if (newHead->uid < 0 || newHead->gid < 0) {
      free_list(result);
      free(data);
      return (struct passwd_entry *)-1;
    }
    newHead->comment = check_empty(d[4]);
    newHead->home = check_empty(d[5]);
    newHead->shell = check_empty(d[6]);
  }
  return result;
}

static inline const char *
maybe_null(const char *x) {
  if (x)
    return x;
  else
    return "";
}

void
write_passwd_file(const char *path, const struct passwd_entry *head) {
  unlink(path);
  int fd = open(path, O_WRONLY | O_CREATE, 0644);
  for (const struct passwd_entry *cur = head; cur; cur = cur->next) {
    printf(fd, "%s:%s:%d:%d:%s:%s:%s\n",
           cur->username,
           maybe_null(cur->password),
           cur->uid,
           cur->gid,
           maybe_null(cur->comment),
           maybe_null(cur->home),
           maybe_null(cur->shell));
  }
  close(fd);
}

void
release_password_entries(struct passwd_entry *head) {
  if (!head)
    return;
  struct passwd_entry *cur = head;
  while (cur->next)
    cur = cur->next;
  free(cur->username);
  free_list(head);
}

static inline char
hex_digit(int x) {
  if (x < 10)
    return x + '0';
  else
    return x - 10 + 'a';
}

void
get_password_repr(const uint8_t hash[32], char repr[65]) {
  repr[64] = 0;
  for (int i = 0; i < 32; ++i) {
    repr[2 * i] = hex_digit(hash[i] >> 4);
    repr[2 * i + 1] = hex_digit(hash[i] & 0xF);
  }
}

struct passwd_entry *
get_entry(struct passwd_entry *list, const char *login) {
  struct passwd_entry *result = 0;
  for (struct passwd_entry *cur = list; cur; cur = cur->next) {
    if (strcmp(login, cur->username) == 0) {
      if (result != 0)
        return (struct passwd_entry *)-1;
      result = cur;
    }
  }
  if (result == 0)
    return 0;
  for (struct passwd_entry *cur = list; cur; cur = cur->next) {
    if (cur->uid == result->uid && cur != result){
      return (struct passwd_entry *)-1;
    }
  }
  return result;
}

struct passwd_entry *
get_entry_pass(struct passwd_entry *list, const char *login, uint8_t pass[32]) {
  struct passwd_entry *res = get_entry(list, login);
  if (res == 0 || res == (struct passwd_entry *)-1)
    return res;
  char repr[65];
  get_password_repr(pass, repr);
  if (res->password != 0 && strcmp(res->password, repr) != 0)
    return 0;
  return res;
}

struct passwd_entry *
get_entry_uid(struct passwd_entry *list, uint uid) {
  struct passwd_entry *res = 0;
  for (struct passwd_entry *cur = list; cur; cur = cur->next) {
    if (cur->uid == uid) {
      if (res != 0) {
        return (struct passwd_entry *)-1;
      }
      res = cur;
    }
  }
  return res;
}

struct passwd_entry *
get_entry_uid_pass(struct passwd_entry *list, uint uid, uint8_t pass[32]) {
  struct passwd_entry *res = get_entry_uid(list, uid);
  if (res == 0 || res == (struct passwd_entry *)-1)
    return res;
  char repr[65];
  get_password_repr(pass, repr);
  if (res->password != 0 && strcmp(res->password, repr) != 0)
    return 0;
  return res;
}
