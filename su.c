#include "user.h"
#include "parse_passwd.h"
#include "sha256.h"

int main(int argc, char **argv) {
  if (argc > 2) {
    printf(2, "incorrect argument format\n");
    exit();
  }
  struct passwd_entry *pass = read_passwd_file("passwords");
  if (pass == (struct passwd_entry *)-1) {
    printf(2, "corrupted passwd file!\n");
    exit();
  }
  struct passwd_entry *ent;
  if (getuid() == 0) {
    if (argv[1]) {
      ent = get_entry(pass, argv[1]);
    } else {
      ent = get_entry_uid(pass, 0);
    }
  } else {
    uint8_t password[32];
    printf(1, "pasword: ");
    read_password(password);
    if (argv[1]) {
      ent = get_entry_pass(pass, argv[1], password);
    } else {
      ent = get_entry_uid_pass(pass, 0, password);
    }
  }
  if (ent == (struct passwd_entry *)-1) {
    printf(2, "corrupted passwd file!\n");
  } else if (ent == 0) {
    printf(2, "login incorrect\n");
  } else {
    setresuid(ent->uid, ent->uid, ent->uid);
    char *arguments[2] = {ent->shell, 0};
    if (arguments[0] == 0)
      arguments[0] = (char *)"/sh";
    if (exec(arguments[0], arguments) == -1) {
      printf(2, "execution of shell failed! Trying to exec() /sh\n");
      arguments[0] = (char *)"/sh";
      if (exec(arguments[0], arguments)== -1){
        printf(2, "exec /sh failed\n");
      }
    }
  }
  release_password_entries(pass);
}
