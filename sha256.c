#include "types.h"
#include "user.h"
#include "sha256.h"

void sha256_init(struct sha256_context *ctx)
{
  memset(ctx, 0, sizeof(*ctx));
  static const uint32_t h_init[8] = {
    0x6A09E667,
    0xBB67AE85,
    0x3C6EF372,
    0xA54FF53A,
    0x510E527F,
    0x9B05688C,
    0x1F83D9AB,
    0x5BE0CD19,
  };
  memcpy(ctx->h, h_init, sizeof h_init);
}

void sha256(const void *data, size_t n, uint8_t md[32])
{
  struct sha256_context c;
  sha256_init(&c);
  sha256_update(&c, data, n);
  sha256_final(md, &c);
  // TODO: clean c
}


#ifdef __GNUC__
#define ROTATE(a,n)  ({ register unsigned int ret;                      \
      asm (                                                             \
        "roll %1,%0"                                                    \
        : "=r"(ret)                                                     \
        : "I"(n), "0"((unsigned int)(a))                                \
        : "cc");                                                        \
      ret;                                                              \
    })
#else
#define ROTATE(a,n) (((a)<<(n))|(((a)&0xffffffff)>>(32-(n))))
#endif

static const uint32_t K256[64] = {
  0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5,
  0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
  0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
  0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
  0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC,
  0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
  0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7,
  0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
  0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
  0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
  0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3,
  0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
  0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5,
  0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
  0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
  0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2,
};

/*
 * FIPS specification refers to right rotations, while our ROTATE macro
 * is left one. This is why you might notice that rotation coefficients
 * differ from those observed in FIPS document by 32-N...
 */

#define Sigma0(x)       (ROTATE((x),30) ^ ROTATE((x),19) ^ ROTATE((x),10))
#define Sigma1(x)       (ROTATE((x),26) ^ ROTATE((x),21) ^ ROTATE((x),7))
#define sigma0(x)       (ROTATE((x),25) ^ ROTATE((x),14) ^ ((x)>>3))
#define sigma1(x)       (ROTATE((x),15) ^ ROTATE((x),13) ^ ((x)>>10))
#define Ch(x,y,z)       (((x) & (y)) ^ ((~(x)) & (z)))
#define Maj(x,y,z)      (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

#define HOST_c2l(c,l)                                                   \
  ({ unsigned int r=*((const uint32_t *)(c));                           \
      asm ("bswapl %0":"=r"(r):"0"(r));                                 \
      (c)+=4; (l)=r;                       })

#define HOST_l2c(l,c)                                                   \
  ({ unsigned int r=(l);                                                \
    asm ("bswapl %0":"=r"(r):"0"(r));                                   \
    *((unsigned int *)(c))=r; (c)+=4; r; })


static void sha256_block_data_order(struct sha256_context *ctx,
                                    const void *in, size_t num)
{
  uint32_t a, b, c, d, e, f, g, h, s0, s1, T1, T2;
  uint32_t X[16], l;
  int i;
  const unsigned char *data = in;

  while (num--) {

    a = ctx->h[0];
    b = ctx->h[1];
    c = ctx->h[2];
    d = ctx->h[3];
    e = ctx->h[4];
    f = ctx->h[5];
    g = ctx->h[6];
    h = ctx->h[7];

    for (i = 0; i < 16; i++) {
      HOST_c2l(data, l);
      T1 = X[i] = l;
      T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
      T2 = Sigma0(a) + Maj(a, b, c);
      h = g;
      g = f;
      f = e;
      e = d + T1;
      d = c;
      c = b;
      b = a;
      a = T1 + T2;
    }

    for (; i < 64; i++) {
      s0 = X[(i + 1) & 0x0f];
      s0 = sigma0(s0);
      s1 = X[(i + 14) & 0x0f];
      s1 = sigma1(s1);

      T1 = X[i & 0xf] += s0 + s1 + X[(i + 9) & 0xf];
      T1 += h + Sigma1(e) + Ch(e, f, g) + K256[i];
      T2 = Sigma0(a) + Maj(a, b, c);
      h = g;
      g = f;
      f = e;
      e = d + T1;
      d = c;
      c = b;
      b = a;
      a = T1 + T2;
    }

    ctx->h[0] += a;
    ctx->h[1] += b;
    ctx->h[2] += c;
    ctx->h[3] += d;
    ctx->h[4] += e;
    ctx->h[5] += f;
    ctx->h[6] += g;
    ctx->h[7] += h;

  }
}

void sha256_update(struct sha256_context *c, const void *data_, size_t len)
{
  const unsigned char *data = data_;
  unsigned char *p;
  uint32_t l;
  size_t n;

  if (len == 0)
    return;

  l = (c->Nl + (((uint32_t) len) << 3)) & 0xffffffff;

  if (l < c->Nl)
    c->Nh++;
  c->Nh += (uint32_t) (len >> 29);      /* might cause compiler warning on
                                         * 16-bit */
  c->Nl = l;

  n = c->num;
  if (n != 0) {
    p = (unsigned char *) c->data;

    if (len >= 64 || len + n >= 64) {
      memcpy(p + n, data, 64 - n);
      sha256_block_data_order(c, p, 1);
      n = 64 - n;
      data += n;
      len -= n;
      c->num = 0;
      memset(p, 0, 64);         /* keep it zeroed */
    } else {
      memcpy(p + n, data, len);
      c->num += (unsigned int) len;
      return;
    }
  }

  n = len / 64;
  if (n > 0) {
    sha256_block_data_order(c, data, n);
    n *= 64;
    data += n;
    len -= n;
  }

  if (len != 0) {
    p = (unsigned char *) c->data;
    c->num = (unsigned int) len;
    memcpy(p, data, len);
  }
}

#define HASH_MAKE_STRING(c,s)   do {                            \
        unsigned long ll;                                       \
        unsigned int  nn;                                       \
        for (nn=0;nn<32/4;nn++)                                 \
        {   ll=(c)->h[nn]; (void)HOST_l2c(ll,(s));   }          \
        } while (0)

void sha256_final(uint8_t * md, struct sha256_context *c)
{
  unsigned char *p = (unsigned char *) c->data;
  size_t n = c->num;

  p[n] = 0x80;                  /* there is always room for one */
  n++;

  if (n > (64 - 8)) {
    memset(p + n, 0, 64 - n);
    n = 0;
    sha256_block_data_order(c, p, 1);
  }
  memset(p + n, 0, 64 - 8 - n);

  p += 64 - 8;
  (void) HOST_l2c(c->Nh, p);
  (void) HOST_l2c(c->Nl, p);
  p -= 64;
  sha256_block_data_order(c, p, 1);
  c->num = 0;
  memset(p, 0, 64);


  HASH_MAKE_STRING(c, md);
}

int read_password(uint8_t hs[32]) {
  int result = 0;
  char buffer[32];
  struct sha256_context c;
  sha256_init(&c);
  
  for (;;) {
    int bytes = read(0, buffer, 32);
    result += bytes;
    if (bytes == -1)
      return -1;
    if (bytes == 0)
      break;
    if (buffer[bytes - 1] == '\n' || buffer[bytes - 1] == '\n') {
      sha256_update(&c, buffer, (size_t) (bytes - 1));
      break;
    }
    sha256_update(&c, buffer, (size_t) bytes);
  }
  sha256_final(hs, &c);
  return result;
}
