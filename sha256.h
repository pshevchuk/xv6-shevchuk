#ifndef SHA256_H
#define SHA256_H
struct sha256_context {
  uint32_t h[8];
  uint32_t Nl, Nh;
  uint32_t data[16];
  unsigned int num;
};                              // size of this structure is 27 * 4 = 108 bytes

void sha256_init(struct sha256_context *ctx);
void sha256_update(struct sha256_context *ctx, const void *data,
                   size_t len);
void sha256_final(uint8_t md[32], struct sha256_context *ctx);
void sha256(const void *data, size_t n, uint8_t md[32]);

int read_password(uint8_t hs[32]);

#endif
