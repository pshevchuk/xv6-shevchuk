#ifndef PIPE_H
#define PIPE_H
#include "defs.h"
#include "spinlock.h"

#define PIPESIZE 512

struct pipe {
  struct spinlock lock;
  struct inode *ip; // inode if fifo
  char data[PIPESIZE];
  uint nread;     // number of bytes read
  uint nwrite;    // number of bytes written
  int readopen;   // read fd is still open
  int writeopen;  // write fd is still open
};
#endif
