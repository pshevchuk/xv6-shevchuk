#ifndef CLONE_H
#define CLONE_H

#define CLONE_VM      0x1
#define CLONE_FILES   0x2
#define CLONE_MUTEXES 0x4
#define CLONE_FORK     (CLONE_VM | CLONE_FILES | CLONE_MUTEXES)
#endif
