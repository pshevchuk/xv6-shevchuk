#include "user.h"

int main(int argc, char **argv) {
  if (argc < 3) {
    printf(2, "incorrect argument format\n");
    exit();
  }
  int n = strlen(argv[1]);
  if (n != 3 && n != 4) {
    printf(2, "incorrect mode\n");
    exit();
  }
  for (int i = 0; i < n; ++i) {
    if (argv[1][i] < '0' || argv[1][i] > '7') {
      printf(2, "incorrect mode\n");
      exit();
    }
  }
  int mode = 0;
  for (int i = 0; i < n; ++i) {
    mode <<= 3;
    mode |= (argv[1][i] - '0');
  }
  for (char **file = &argv[2]; *file; ++file) {
    if (chmod(*file, mode) < 0) {
      printf(2, "chmod %s failed!\n", *file);      
    }
  }
}
