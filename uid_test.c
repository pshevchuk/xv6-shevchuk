#include "user.h"

int main(int argc, char **argv) {
  int uid, euid, suid;
  getresuid(&uid, &euid, &suid);
  if (uid || euid || suid) {
    printf(2, "test 1 failed: %d %d %d\n", uid, euid, suid);
    exit();
  }
  setresuid(1, 1000, 100);
  getresuid(&uid, &euid, &suid);
  if ((uid != 1) || (euid != 1000) || (suid != 100)) {
    printf(2, "test 2 failed: %d %d %d\n", uid, euid, suid);
    exit();
  }
  if (setresuid(0, 0, 0) != -1) {
    printf(2, "incorrect setuid is not failed!!!\n");
    exit();
  }
  getresuid(&uid, &euid, &suid);
  if ((uid != 1) || (euid != 1000) || (suid != 100)) {
    printf(2, "test 3 failed: %d %d %d\n", uid, euid, suid);
    exit();
  }
  setresuid(100, 1, 1000);
  getresuid(&uid, &euid, &suid);
  if ((uid != 100) || (euid != 1) || (suid != 1000)) {
    printf(2, "test 4 failed: %d %d %d\n", uid, euid, suid);
    exit();
  }
}
