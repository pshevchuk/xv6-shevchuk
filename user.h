#include "types.h"
#include "clone.h"
struct stat;

int fork(void); // well almost
// system calls

int clone(void (*)(void *), void *, int, void *);
int exit(void) __attribute__((noreturn));
int wait(void);
int pipe(int*);
int write(int, const void*, int);
int read(int, void*, int);
int close(int);
int kill(int);
int exec(const char*, char**);
int open(const char*, int, ...);
int mknod(const char*, int, short, short);
int unlink(const char*);
int fstat(int fd, struct stat*);
int link(const char*, const char*);
int mkdir(const char*, int);
int chdir(const char*);
int dup(int);
int getpid(void);
char* sbrk(int);
int sleep(int);
int uptime(void);
int halt(void);
int mkfifo(const char *, int mode);
int getresuid(int *, int *, int *);
int setresuid(int, int, int);
int stat(const char *, struct stat *);
int chmod(const char *path, int);
int chown(const char *, int uid);
int fchmod(int, int);
int fchown(int, int);
int mutex_anon();
int mutex_open(const char *, int, ...);
int mutex_lock(int);
int mutex_unlock(int);
int mutex_try_lock(int);
int mutex_close(int);
void sched_yield();

// ulib.c
char* strcpy(char*, char*);
void *memmove(void*, const void*, int);
char* strchr(const char*, char c);
int strcmp(const char*, const char*);
void printf(int, char*, ...);
char* gets(char*, int max);
uint strlen(char*);
void* memset(void*, int, uint);
int memcmp(const void *, const void *, size_t);
void* malloc(uint);
void free(const void*);
int atoi(const char*);
char *read_line(int fd);
int getuid();
int geteuid();
int getsuid();

#define memcpy memmove
