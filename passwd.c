#include "user.h"
#include "parse_passwd.h"
#include "sha256.h"

void
error(const char *msg) {
  printf(2, "%s\n", msg);
  exit();
}

int
try_change(struct passwd_entry *head, const char *login) {
  uint8_t old_password[32], new_password[2][32];
  static const char null_sha[] = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
  uid_t uid = getuid();
  if (uid != 0) {
    printf(1, "current password: ");
    if (read_password(old_password) == -1)
      error("read error");
  }
  printf(1, "new password: ");
  if (read_password(new_password[0]) == -1)
    error("read error");
  printf(1, "repeat new password: ");
  if (read_password(new_password[1]) == -1)
    error("read error");
  if (memcmp(new_password[1], new_password[0], 32) != 0) {
    return 1;
  }
  struct passwd_entry *exp;
  if (uid == 0) {
    if (login)
      exp = get_entry(head, login);
    else
      exp = get_entry_uid(head, uid);
  } else {
    if (login)
      exp = get_entry_pass(head, login, old_password);
    else
      exp = get_entry_uid_pass(head, uid, old_password);
  }
  if (exp == 0) {
    return 2;
  } else if (exp == (struct passwd_entry *)-1) {
    return 1;
  } else {
    char new_password_repr[65];
    get_password_repr(new_password[0], new_password_repr);
    if (strcmp(new_password_repr, null_sha) == 0)
      exp->password = 0;
    else
      exp->password = new_password_repr;
    write_passwd_file("passwords", head);
    return 0; 
  }
}

int main(int argc, char **argv) {
  if (argc > 2) {
    printf(2, "incorrect arguments!\n");
    exit();
  }
  struct passwd_entry *head = read_passwd_file("passwords");
  if (head == (struct passwd_entry *)-1) {
    error("failed to read passwd file");
  }
  for (int i = 0; i < 3; ++i) {
    switch (try_change(head, argv[1])) {
    case 1:
      printf(2, "new password are not equal\n");
      break;
    case 2:
      printf(2, "old password error\n");
      break;
    case 3:
      error("no such login");
    case 0:
      exit();
    }
  }
}
