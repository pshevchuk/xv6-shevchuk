#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"
#include "file.h"
#include "mutex.h"
#include "clone.h"

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
  struct file_table ftbl[NPROC];
  struct mutex_table mtbl[NPROC];
  struct page_table ptbl[NPROC];
} ptable;

static struct file_table *
allocate_file_table() {
  for (int i = 0; i < NPROC; ++i) {
    if (ptable.ftbl[i].ref == 0) {
      ++ptable.ftbl[i].ref;
      memset(ptable.ftbl[i].ofile, 0, sizeof ptable.ftbl[i].ofile);
      return &ptable.ftbl[i];
    }
  }
  panic("allocate_file_table");
}

static struct file_table *
clone_file_table(struct file_table *table) {
  struct file_table *res = allocate_file_table();
  for (int i = 0; i < NOFILE; ++i) {
    if (table->ofile[i])
      res->ofile[i] = filedup(table->ofile[i]);
  }
  return res;
}

static inline struct file_table *
dup_file_table(struct file_table *table) {
  ++table->ref;
  return table;
}

static inline void
close_file_table(struct file_table *tbl) {
  --tbl->ref;
}

static struct mutex_table *
allocate_mutex_table() {
  for (int i = 0; i < NPROC; ++i) {
    if (ptable.mtbl[i].ref == 0) {
      ++ptable.mtbl[i].ref;
      memset(ptable.mtbl[i].omutex, 0, sizeof ptable.mtbl[i].omutex);
      return &ptable.mtbl[i];
    }
  }
  panic("allocate_mutex_table");
}

static struct mutex_table *
clone_mutex_table(struct mutex_table *table) {
  struct mutex_table *res = allocate_mutex_table();
  for (int i = 0; i < NOMUTEX; ++i) {
    if (table->omutex[i])
      res->omutex[i] = mutexdup(table->omutex[i]);
  }
  return res;
}

static inline struct mutex_table *
dup_mutex_table(struct mutex_table *table) {
  ++table->ref;
  return table;
}

static inline void
close_mutex_table(struct mutex_table *tbl) {
  --tbl->ref;
}

static struct page_table *
allocate_page_table() {
  for (int i = 0; i < NPROC; ++i) {
    if (ptable.ptbl[i].ref == 0) {
      ptable.ptbl[i].ref = 1;
      ptable.ptbl[i].pgdir = 0;
      ptable.ptbl[i].sz = 0;
      return &ptable.ptbl[i];
    }
  }
  panic("allocate_page_table");
}

static struct page_table *
clone_page_table(struct page_table *table) {
  struct page_table *res = allocate_page_table();
  res->pgdir = copyuvm(table->pgdir, table->sz);
  if (res->pgdir == 0) {
    --res->ref;
    return 0;
  }
  res->sz = table->sz;
  return res;
}

static inline struct page_table *
dup_page_table(struct page_table *table) {
  ++table->ref;
  return table;
}

void
close_page_table(struct page_table *table) {
  if (0 == --table->ref) {
    freevm(table->pgdir);
  }
}

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  initproc = p;
  p->tbl = allocate_page_table();
  p->tbl->sz = PGSIZE;
  p->tbl->pgdir = setupkvm();
  if (p->tbl->pgdir == 0)
    panic("userinit: out of memory?");
  inituvm(p->tbl->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->tbl->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S
  p->suid = p->euid = p->uid = 0;
  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");
  p->ftbl = allocate_file_table();
  p->mtbl = allocate_mutex_table();
  p->state = RUNNABLE;
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  
  sz = proc->tbl->sz;
  if(n > 0){
    if((sz = allocuvm(proc->tbl->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->tbl->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->tbl->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
clone(int flags, void *childstack) {

  int pid;
  struct proc *np;

// Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  acquire(&ptable.lock);
  if (flags & CLONE_VM) {
    np->tbl = clone_page_table(proc->tbl);
    // Copy process state from p.
    if(np->tbl == 0){
      kfree(np->kstack);
      np->kstack = 0;
      np->state = UNUSED;
      return -1;
    }
  } else {
    np->tbl = dup_page_table(proc->tbl);
  }
  release(&ptable.lock);
  np->parent = proc;
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
  if (childstack)
    np->tf->esp = (int)childstack;

  if (flags & CLONE_FILES) {
    np->ftbl = clone_file_table(proc->ftbl);
  } else {
    np->ftbl = dup_file_table(proc->ftbl);
  }

  if (flags & CLONE_MUTEXES) {
    np->mtbl = clone_mutex_table(proc->mtbl);
  } else {
    np->mtbl = dup_mutex_table(proc->mtbl);
  }
  np->cwd = idup(proc->cwd);
 
  pid = np->pid;
  np->state = RUNNABLE;
  safestrcpy(np->name, proc->name, sizeof(proc->name));
  np->uid = proc->uid;
  np->euid = proc->euid;
  np->suid = proc->suid;
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  struct proc *p;

  if(proc == initproc)
    panic("init exiting");


  for (int i = 0; i < NOMUTEX; ++i) {
    if (proc->mtbl->omutex[i]) {
      mutexunlock(proc->mtbl->omutex[i]); // just try
    }
  }
  if (proc->mtbl->ref == 1) {
    for (int i = 0; i < NOMUTEX; ++i) {
      if (proc->mtbl->omutex[i]) {
        mutexclose(proc->mtbl->omutex[i]); // just try
      }
    }
  }

  if (proc->ftbl->ref == 1) {
    for (int i = 0; i < NOFILE; ++i) {
      if (proc->ftbl->ofile[i])
        fileclose(proc->ftbl->ofile[i]);
    }
  }
  iput(proc->cwd);
  proc->cwd = 0;

  acquire(&ptable.lock);
  close_mutex_table(proc->mtbl);
  close_file_table(proc->ftbl);
  
  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  sched();
  panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        close_page_table(p->tbl);
        p->state = UNUSED;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        p->uid = 0;
        p->euid = 0;
        p->suid = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
  struct proc *p;

  for(;;){
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);

  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
  proc->state = RUNNABLE;
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    initlog();
  }
  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
    acquire(&ptable.lock);  //DOC: sleeplock1
    release(lk);
  }

  // Go to sleep.
  proc->chan = chan;
  proc->state = SLEEPING;
  sched();

  // Tidy up.
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  }
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

void
wakeup_one(void *chan) {
  acquire(&ptable.lock);
  for(struct proc *p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
    if(p->state == SLEEPING && p->chan == chan) {
      p->state = RUNNABLE;
      break;
    }
  }
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}

_Bool
can_execute(struct inode *ip) {
  if (proc->euid == 0)
    return 1;
  if (proc->euid == ip->uid)
    return (ip->mode & 0100);
  else
    return (ip->mode & 0001);
}

_Bool
can_read(struct inode *ip) {
  if (proc->euid == 0)
    return 1;
  if (proc->euid == ip->uid)
    return (ip->mode & 0400);
  else
    return (ip->mode & 0004);
}

_Bool
can_write(struct inode *ip) {
  if (proc->euid == 0)
    return 1;
  if (proc->euid == ip->uid)
    return (ip->mode & 0200);
  else
    return (ip->mode & 0002);
}
