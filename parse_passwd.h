#ifndef PARSE_PASSWD_H
#define PARSE_PASSWD_H
#include "types.h"

struct passwd_entry {
  struct passwd_entry *next;
  char *username;
  char *password;
  char *comment;
  char *home;
  char *shell;
  int uid;
  int gid;
};

struct passwd_entry *read_passwd_file(const char *passwd_path);
struct passwd_entry *get_entry(struct passwd_entry *list, const char *login);
struct passwd_entry *get_entry_pass(struct passwd_entry *list, const char *login, uint8_t pass[32]);
struct passwd_entry *get_entry_uid(struct passwd_entry *list, uint uid);
struct passwd_entry *get_entry_uid_pass(struct passwd_entry *list, uint uid, uint8_t pass[32]);

void write_passwd_file(const char *path, const struct passwd_entry *head);

void release_password_entries(struct passwd_entry *head);

void get_password_repr(const uint8_t hash[32], char repr[65]);

#endif 
