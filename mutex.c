#include "mutex.h"
#include "param.h"
#include "defs.h"
#include "proc.h"

static struct {
  struct spinlock lock; // perhaps the global lock for all mutexes is bad ... neverminds
  struct mutex mutex[NMUTEX];
} mtable;

void mutexinit(void) {
  initlock(&mtable.lock, "mtable");
  for (int i = 0; i < NMUTEX; ++i) {
    initlock(&mtable.mutex[i].lock, "mutex");
  }
}

struct mutex *
mutexalloc(void) {
  acquire(&mtable.lock);
  struct mutex *result = 0;
  for (int i = 0; i < NMUTEX; ++i) {
    if (mtable.mutex[i].ref == 0) {
      mtable.mutex[i].ref = 1;
      result = &mtable.mutex[i];
      break;
    }
  }
  release(&mtable.lock);
  return result;
}

struct mutex *
mutexdup(struct mutex *mtx) {
  acquire(&mtx->lock);
  if (mtx->ref < 1)
    panic("mutexdup");
  ++mtx->ref;
  release(&mtx->lock);
  return mtx;
}

int
mutexclose(struct mutex *mtx) {
  acquire(&mtx->lock);
  if (mtx->ref < 1)
    panic("mutexclose");
  if (mtx->ref == 1) {
    if (mtx->ip != 0) {
      iput(mtx->ip);
      mtx->ip = 0;
    }
    mtx->owner = 0;
    acquire(&mtable.lock);
    --mtx->ref;
    release(&mtx->lock);
    release(&mtable.lock);
  } else {
    --mtx->ref;
    release(&mtx->lock);
  }
  return 1;
}

int
mutextrylock(struct mutex *mtx) {
  acquire(&mtx->lock);
  int result = mtx->owner == 0;
  if (result)
    mtx->owner = proc;
  release(&mtx->lock);
  return result;
}

int
mutexlock(struct mutex *mtx) {
  acquire(&mtx->lock);
  while (mtx->owner != 0) {
    sleep(mtx, &mtx->lock);
  }
  mtx->owner = proc;
  release(&mtx->lock);
  return 1;
}

int
mutexunlock(struct mutex *mtx) {
  acquire(&mtx->lock);
  if (mtx->owner != proc) {
    release(&mtx->lock);
    return 0;
  }
  mtx->owner = 0;
  wakeup_one(mtx); 
  release(&mtx->lock);
  return 1;
}

int mdalloc(struct mutex *mtx) {
  for (int md = 0; md < NOMUTEX; ++md) {
    if (proc->mtbl->omutex[md] == 0) {
      proc->mtbl->omutex[md] = mtx;
      return md;
    }
  }
  return -1;
}
