#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "defs.h"
#include "x86.h"
#include "elf.h"
#include "file.h"

#ifndef MAX_EXEC_RECURSION
#define MAX_EXEC_RECURSION 5
#endif

static int
_exec(char *path, char **argv, int recursion_limit);

int
exec(char *path, char **argv)
{
  return _exec(path, argv, MAX_EXEC_RECURSION);
}

static inline int
_exec_shebang(struct inode *ip, char **argv, int recur_limit);

static int
_exec(char *path, char **argv, int recursion_limit)
{
  char *s, *last;
  int i, off;
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir;

  if (recursion_limit-- == 0)
    return -1;
  if((ip = namei(path)) == 0)
    return -1;
  ilock(ip);
  if (ip->mode & 04000) {
    proc->euid = ip->uid;
  }
  
  if (!can_execute(ip)) {
    iunlock(ip);
    return -1;
  }
  
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf) || elf.magic != ELF_MAGIC)
    // only ip shall be unlocked, but ip will be freed by _exec_shebang();
    return _exec_shebang(ip, argv, recursion_limit);

  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
  ip = 0;

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;

  ustack[0] = 0xffffffff;  // fake return PC
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));

  // Commit to the user image.

  pde_t *old_pgdir = proc->tbl->pgdir;
  proc->tbl->pgdir = pgdir;
  proc->tbl->sz = sz;
  proc->tf->eip = elf.entry;  // main
  proc->tf->esp = sp;

  proc->suid = proc->euid;
  switchuvm(proc);
  freevm(old_pgdir);
  return 0;
 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip)
    iunlockput(ip);
  return -1;
}

static inline int
isspace(int c) {
  return c == ' ' || c == '\t' || c == '\v' || c == '\n';
}

char *next_word(char **strp) {
  if (**strp == '\0')
    return 0;
  while (isspace(**strp) && **strp != '\n')
    ++*strp;
  if (**strp == '\n')
    return 0;
  char *result = *strp;
  while (!isspace(**strp))
    ++*strp;
  if (**strp == '\n') {
    **strp = '\0';
  } else {
    **strp = '\0';
    ++*strp;
  }
  return result;
}

static inline int
_exec_shebang(struct inode *ip, char **argv, int recur_limit) {
  char magic[2];
  char *new_argv[MAXARG+1];
  int res = -1;
  char *first_line;
  int bytes;
  int i;
  static char default_interpreter_path[] = "sh";

  if (readi(ip, magic, 0, 2) < 2)
    goto fail;
  if (magic[0] != '#' || magic[1] != '!')
    goto fail;
  first_line = kalloc();
  bytes = readi(ip, first_line, 2, 4096);

  int actual_bytes;
  for (actual_bytes = 0; actual_bytes < bytes; ++actual_bytes) {
    if (first_line[actual_bytes] == '\n')
      break;
  }
  if (actual_bytes == 4096)
    goto fail_first_line;
  char *cur = first_line;
  char *path = next_word(&cur);
  if (path == 0) {
    path = default_interpreter_path;
  }
  new_argv[0] = path;
  int argvp = 1;
  while ((new_argv[argvp] = next_word(&cur)) != 0)
    ++argvp;

  for (i = 0; argv[i]; ++i) {
    if (i + argvp  == MAXARG)
      goto fail_first_line;
    new_argv[i + argvp] = argv[i];
  }
  new_argv[i + argvp] = 0;
  iunlockput(ip);
  res = _exec(path, new_argv, recur_limit);
  kfree(first_line);
  return res;

fail_first_line:
  kfree(first_line);
fail:
  iunlockput(ip);
  return -1;
}
