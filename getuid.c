#include "user.h"

int main(int argc, char **argv) {
  int uid, euid, suid;

  getresuid(&uid, &euid, &suid);
  printf(1, "uid = %d; effective uid = %d; saved uid = %d\n", uid, euid, suid);
}
