#include "types.h"
#include "user.h"
#include "sha256.h"
#include "parse_passwd.h"
#include "fcntl.h"

int main(int argc, char **argv) {
  if (argc != 1) {
    printf(2, "incorrect argument format!!!\n");
    exit();
  }
  if (geteuid() != 0) {
    printf(2, "login shall be run as root!\n");
    exit();
  }
  setresuid(0, 0, 0);
  close(0);
  if (open("console", O_RDWR) < 0) {
    printf(2, "open console failed!\n");
    exit();
  }
  close(1);
  close(2);
  dup(0);
  dup(0);
  struct passwd_entry *head = read_passwd_file("passwords");
  if (head == (struct passwd_entry *)-1) {
    printf(2, "passwd file corrupted!\n");
    exit();
  }
  printf(1, "xv6 login: ");
  char *login = read_line(0);
  printf(1, "password: ");
  uint8_t password[32];
  read_password(password);
  struct passwd_entry *exp = get_entry_pass(head, login, password);
  free(login);
  if (exp == 0) {
    printf(2, "login incorrect!\n");
    exit();
  }
  if (exp == (struct passwd_entry *)-1) {
    printf(2, "passwdfile corrupted\n");
    exit();
  }
  if (exp->shell == 0)
    exp->shell = "/sh";
  char *arguments[2] = {exp->shell, 0};
  setresuid(exp->uid, exp->uid, exp->uid);
  if (chdir(exp->home) == -1) {
    printf(2, "chdir home failed! trying /\n");
    if (chdir("/") == -1) {
      printf(2, "chdir / failed!\n");
      exit();
    }
      
  }
  if (exec(arguments[0], arguments) == -1) {
    printf(2, "execution of shell failed! Trying to exec() /sh\n");
    arguments[0] = (char *)"/sh";
    if (exec(arguments[0], arguments)== -1){
      printf(2, "exec /sh failed\n");
    }
  }
}
