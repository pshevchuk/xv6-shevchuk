#include "user.h"
#include "parse_passwd.h"

int main(int argc, char **argv) {
  struct passwd_entry *passwd = read_passwd_file("users");
  if (passwd == (struct passwd_entry *)-1) {
    printf(2, "passwd file corrupted!\n");
    exit();
  }
  if (argc < 3) {
    printf(2, "incorrect argument format!\n");
    exit();
  }
  struct passwd_entry *ent = get_entry(passwd, argv[1]);
  if (ent == (struct passwd_entry *)-1) {
    printf(2, "passwd file corrupted!\n");
    exit();
  }
  if (ent ==  0) {
    printf(2, "no such user!\n");
    exit();
  }
  
  for (char **file = &argv[2]; *file; ++file) {
    if (chown(*file, ent->uid) == -1) {
      printf(2, "chown %s failed!\n", *file);
    }
  }
  release_password_entries(passwd);
}
