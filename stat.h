#ifndef STAT_H
#define STAT_H
#define T_DIR   1   // Directory
#define T_FILE  2   // File
#define T_DEV   3   // Device
#define T_FIFO  4   // Fifo
#define T_MUTEX 5

struct stat {
  short type;  // Type of file
  int dev;     // File system's disk device
  uint ino;    // Inode number
  short nlink; // Number of links to file
  uint size;   // Size of file in bytes
  uint mode;   // access mode
  uint uid;    // user id of the owner
};
#endif
