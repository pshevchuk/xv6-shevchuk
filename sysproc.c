#include "types.h"
#include "x86.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "clone.h"

int
sys_clone(void)
{
  int flags;
  void *stack;
  if (argint(0, &flags) < 0 || argint(1, (int *)&stack) < 0) {
    return -1;
  }
  return clone(flags, stack);
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->tbl->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;
  
  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_halt(void)
{
  if (proc->euid != 0)
    return -1;
  return -2;
}

int
sys_getresuid(void)
{
  int *uid, *euid, *suid;
  if ((argptr(0, (void*)&uid, sizeof(*uid)) < 0)
      || (argptr(1, (void*)&euid, sizeof(*euid)) < 0)
      || (argptr(2, (void*)&suid, sizeof(*suid)) < 0)) {
    return -1;
  }
  *uid = proc->uid;
  *euid = proc->euid;
  *suid = proc->suid;
  return 0;
}

static inline int check_new_uid(int x) {
  return proc->uid == x || proc->euid == x || proc->suid == x;
}

int sys_setresuid(void)
{
  int uid, euid, suid;
  if (argint(0, &uid) < 0 || argint(1, &euid) < 0 || argint(2, &suid) < 0) {
    return -1;
  }
  if (proc->euid != 0 && (!check_new_uid(uid) || !check_new_uid(euid) || !check_new_uid(suid)))
    return -1;
  proc->uid  = uid;
  proc->euid = euid;
  proc->suid = suid;
  return 0;
}

int sys_sched_yield(void)
{
  yield();
  return 0;
}
