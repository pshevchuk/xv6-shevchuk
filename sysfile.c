//
// File-system system calls.
// Mostly argument checking, since we don't trust
// user code, and calls into file.c and fs.c.
//

#include "types.h"
#include "defs.h"
#include "param.h"
#include "stat.h"
#include "mmu.h"
#include "proc.h"
#include "fs.h"
#include "file.h"
#include "fcntl.h"
#include "pipe.h"
#include "mutex.h"

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
    return -1;
  if(fd < 0 || fd >= NOFILE || (f=proc->ftbl->ofile[fd]) == 0)
    return -1;
  if(pfd)
    *pfd = fd;
  if(pf)
    *pf = f;
  return 0;
}

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ftbl->ofile[fd] == 0){
      proc->ftbl->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
}

static inline _Bool
check_permissions(int mode) {
  return !(mode & ~0xFFF);
}

int
sys_dup(void)
{
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
    return -1;
  if((fd=fdalloc(f)) < 0)
    return -1;
  filedup(f);
  return fd;
}

int
sys_read(void)
{
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
    return -1;
  return fileread(f, p, n);
}

int
sys_write(void)
{
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
    return -1;
  return filewrite(f, p, n);
}

int
sys_close(void)
{
  int fd;
  struct file *f;
  
  if(argfd(0, &fd, &f) < 0)
    return -1;
  proc->ftbl->ofile[fd] = 0;
  fileclose(f);
  return 0;
}

int
sys_fstat(void)
{
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
    return -1;
  
  return filestat(f, st);
}

int
sys_stat(void)
{
  char *path;
  struct stat *st;
  
  if(argstr(0, &path) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
    return -1;
  struct inode *ip = namei(path);
  if (ip == 0)
    return -1;
  ilock(ip);
  stati(ip, st);
  iunlockput(ip);
  return 0;
}

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
    return -1;
  if((ip = namei(old)) == 0)
    return -1;

  begin_trans();

  ilock(ip);
  if(ip->type == T_DIR){
    iunlockput(ip);
    commit_trans();
    return -1;
  }

  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
  ilock(dp);
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
    iunlockput(dp);
    goto bad;
  }
  iunlockput(dp);
  iput(ip);

  commit_trans();

  return 0;

bad:
  ilock(ip);
  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);
  commit_trans();
  return -1;
}

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
}

//PAGEBREAK!
int
sys_unlink(void)
{
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
    return -1;
  if((dp = nameiparent(path, name)) == 0)
    return -1;

  begin_trans();

  ilock(dp);
  
  if (!can_write(dp))
    goto bad;
  
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;
  
  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
  ilock(ip);
  if (!can_write(ip)) {
    iunlockput(ip);
    goto bad;
  }

  if (proc->euid != 0 && (proc->euid != ip->uid && (dp->mode & 01000))) {
    iunlockput(ip);
    goto bad;
  }
    
  if(ip->nlink < 1)
    panic("unlink: nlink < 1");
  if(ip->type == T_DIR && !isdirempty(ip)){
    iunlockput(ip);
    goto bad;
  }
  if (ip->type == T_FIFO) {
    ip->pipe = 0; // pipe is either not allocated or will be deallocated later
  }
  memset(&de, 0, sizeof(de));
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
    panic("unlink: writei");
  if(ip->type == T_DIR){
    dp->nlink--;
    iupdate(dp);
  }
  iunlockput(dp);

  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);

  commit_trans();

  return 0;

bad:
  iunlockput(dp);
  commit_trans();
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor, uint uid, uint mode)
{
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
    return 0;
  ilock(dp);

  if (!can_write(dp)) {
    iunlockput(dp);
    return 0;
  }
  if((ip = dirlookup(dp, name, &off)) != 0){
    iunlockput(dp);
    ilock(ip);
    if(type == T_FILE && ip->type == T_FILE)
      return ip;
    iunlockput(ip);
    return 0;
  }

  if((ip = ialloc(dp->dev, type)) == 0)
    panic("create: ialloc");

  ilock(ip);
  ip->major = major;
  ip->minor = minor;
  ip->nlink = 1;
  ip->uid = uid;
  ip->mode = mode;
  iupdate(ip);

  if(type == T_DIR){  // Create . and .. entries.
    dp->nlink++;  // for ".."
    iupdate(dp);
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
      panic("create dots");
  }

  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}

int
sys_open(void)
{
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
    return -1;
  ip = namei(path);
  if (ip == 0) {
    if(omode & O_CREATE){
      int mode;
      if (argint(2, &mode) < 0 || !check_permissions(mode))
        return -1;
      begin_trans();
      ip = create(path, T_FILE, 0, 0, proc->euid, (uint)mode);
      commit_trans();
      if(ip == 0) {
        return -1;
      }
    } else {
      return -1;
    }
  } else {
    ilock(ip);
  }
  // by this moment we hold ip
  if (ip->type == T_MUTEX) {
    iunlockput(ip);
    return -1;
  }
    
  if(ip->type == T_DIR && omode != O_RDONLY){
    iunlockput(ip);
    return -1;
  }
  if (((omode & O_WRONLY) || (omode & O_RDWR)) && !can_write(ip)) {
    iunlockput(ip);
    return -1;
  }
  if (!(omode & O_WRONLY) && !can_read(ip)) {
    iunlockput(ip);
    return -1;
  }
     
  if (ip->type == T_FIFO) {
    if (omode & O_RDWR)
      goto fail_put;
    if (ip->pipe == 0) { 
      ip->pipe = fifo_pipe_alloc(ip);
      if (ip->pipe == 0)
        goto fail_put;
    }
    struct pipe *pipe = ip->pipe;
    /* the situation with locks and channels here is a bit messy, so
       pipe->lock is used to protect pipe->readopen AND pipe->writeopen
       &pipe->readopen is used to wait for the reader */

    acquire(&pipe->lock);
    iunlock(ip);
    if (omode == O_RDONLY) {
      struct file *f = fifo_file_alloc(pipe, 0);
      while (pipe->writeopen == 0) {
        wakeup(&pipe->readopen);
        sleep(&pipe->writeopen, &pipe->lock);
      }
      wakeup(&pipe->readopen);
      release(&pipe->lock);
      fd = fdalloc(f);
    } else {
      struct file *f = fifo_file_alloc(pipe, 1);
      while (pipe->readopen == 0) {
        wakeup(&pipe->writeopen);
        sleep(&pipe->readopen, &pipe->lock);
      }
      wakeup(&pipe->writeopen);
      release(&pipe->lock);
      fd = fdalloc(f);
    }
    if (fd < 0)
      goto fail;
    return fd;
fail_put:
    iunlockput(ip);
    return -1;
fail:
    iunlock(ip);
    return -1;
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
    if(f)
      fileclose(f);
    iunlockput(ip);
    return -1;
  }
  iunlock(ip);

  f->type = FD_INODE;
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
  return fd;
}

int
sys_mkdir(void)
{
  char *path;
  struct inode *ip;
  int mode;

  begin_trans();
  if(argstr(0, &path) < 0 || argint(1, &mode) < 0 || !check_permissions(mode) || (ip = create(path, T_DIR, 0, 0, proc->euid, (uint)mode)) == 0){
    commit_trans();
    return -1;
  }
  iunlockput(ip);
  commit_trans();
  return 0;
}

int
sys_mknod(void)
{
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  int mode;
  
  begin_trans();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &mode)  < 0      ||
     argint(2, &major) < 0      ||
     argint(3, &minor) < 0      ||
     !check_permissions(mode)   ||
     (ip = create(path, T_DEV, major, minor, proc->euid, mode)) == 0){
    commit_trans();
    return -1;
  }
  iunlockput(ip);
  commit_trans();
  return 0;
}

int
sys_chdir(void)
{
  char *path;
  struct inode *ip;

  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0)
    return -1;
  ilock(ip);
  if(ip->type != T_DIR || !can_execute(ip)){
    iunlockput(ip);
    return -1;
  }
  iunlock(ip);
  iput(proc->cwd);
  proc->cwd = ip;
  return 0;
}

int
sys_exec(void)
{
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
}

int
sys_pipe(void)
{
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
    return -1;
  if(pipealloc(&rf, &wf) < 0)
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
    if(fd0 >= 0)
      proc->ftbl->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  fd[0] = fd0;
  fd[1] = fd1;
  return 0;
}

int
sys_mkfifo(void)
{
  char *path;
  int mode;
  struct inode *ip;

  if (argstr(0, &path) < 0 || argint(1, &mode) < 0 || !check_permissions(mode))
    return -1;
  begin_trans();
  ip = create(path, T_FIFO, 0, 0, proc->uid, mode);
  if(ip == 0) {
    commit_trans();
    return -1;
  }
  iunlockput(ip);
  commit_trans();
  return 0;
}

static int
ichmod(struct inode *ip, int mode) {
  if (ip == 0)
    return -1;
  begin_trans();
  ilock(ip);
  if (proc->euid != 0 && proc->euid != ip->uid) {
    iunlockput(ip);
    commit_trans();
    return -1;
  }
  ip->mode = mode;
  iupdate(ip);
  iunlockput(ip);
  commit_trans();
  return 0;
}

static int
ichown(struct inode *ip, int uid) {
  if (proc->euid != 0)
    return -1;
  if (ip == 0)
    return -1;
  begin_trans();
  ilock(ip);
  ip->uid = uid;
  iupdate(ip);
  iunlockput(ip);
  commit_trans();
  return 0;
}

int
sys_chmod(void)
{
  char *path;
  int mode;
  if (argstr(0, &path) < 0 || argint(1, &mode) < 0 || !check_permissions(mode))
    return -1;
  struct inode *ip = namei(path);
  if (ip == 0)
    return -1;
  return ichmod(ip, mode);
}

int
sys_chown(void)
{
  char *path;
  int uid;
  if (argstr(0, &path) < 0 || argint(1, &uid) < 0 || uid < 0)
    return -1;
  
  return ichown(namei(path), uid);
}
  
int
sys_fchmod(void)
{
  struct file *f;
  int mode;
  
  if(argfd(0, 0, &f) < 0 || argint(1, &mode) < 0 || !check_permissions(mode))
    return -1;
  return ichmod(f->ip, mode);
}

int
sys_fchown(void)
{
  struct file *f;
  int uid;
  
  if(argfd(0, 0, &f) < 0 || argint(1, &uid) < 0)
    return -1;
  return ichown(f->ip, uid);
}


int sys_mutex_open() {
  char *path;
  int flags;
  if (argstr(0, &path) < 0 || argint(1, &flags) < 0)
      return -1;
  struct inode *ip = namei(path);
  if (ip != 0) {
    if ((flags & MTX_OPEN) == 0) {
      return -1;
    }
    ilock(ip);
    if (ip->type != T_MUTEX || !can_execute(ip)) {
      iunlockput(ip);
      return -1;
    }   
  } else {
    if ((flags & MTX_CREATE) == 0) {
      return -1;
    }
    int mode;
    if (argint(2, &mode) < 0 || !check_permissions(mode))
      return -1;
    begin_trans();
    ip = create(path, T_MUTEX, 0, 0, proc->euid, (uint)mode);
    commit_trans();
    if (ip == 0) {
      return -1;
    }
  }
  struct mutex *mtx;
  if (ip->mutex == 0) {
    ip->mutex = mutexalloc();
    if (ip->mutex == 0) {
      iunlockput(ip);
      return -1;
    }
    ip->mutex->ip = idup(ip);
    mtx = ip->mutex;
  } else {
    mtx = mutexdup(ip->mutex);
  }
  iunlockput(ip);
  int md = mdalloc(mtx);
  if (md == -1) {
    mutexclose(mtx);
  }
  return md;
}
