#include "user.h"
#include "fcntl.h"
#define ATTEMPTS 10000
void dummy() {
  for (int j = 0; j < 5000; ++j) asm("nop");
}

void mutextest_op(int m, int p[2]) {
  close(p[0]);
  for (int i = 0; i < ATTEMPTS; ++i) {
      mutex_lock(m);
      printf(p[1], "a");
      dummy();
      printf(p[1], "b");
      mutex_unlock(m);
      dummy();
  }
  exit();
}

void mutextest_cmp(const char *name, int p[2], int pid[2]) {
  close(p[1]);
  char buf;
  char pr = 0;
  while (read(p[0], &buf, 1)) {
    if (buf == pr) {
      printf(2, "mutex test '%s' failed!\n", name);
      kill(pid[0]);
      kill(pid[1]);
      exit();
    }
    pr = buf;
  }
  close(p[0]);
  wait();
  wait();
  printf(2, "mutex test '%s' passed!\n", name);
}

struct thread_data {
  int mutex;
  volatile int current_value;
  int pipe_end;
} tdata;

void thread_function(void *arg) {
  close(*(int *)arg);
  for (int i = 0; i < ATTEMPTS; ++i) {
    mutex_lock(tdata.mutex);
    dummy();
    int val = ++tdata.current_value;
    write(tdata.pipe_end, &val, sizeof val);
    dummy();
    mutex_unlock(tdata.mutex);
  }
}

int main() {
  int p[2];
  int m = mutex_anon();
  pipe(p);
  int pid[2];
  for (int i = 0; i < 2; ++i) {
    pid[i] = fork();
    if (pid[i] != 0)
      continue;
    mutextest_op(m, p);
  }
  mutextest_cmp("anon", p, pid);
  pipe(p);
  mutex_close(m);
  for (int i = 0; i < 2; ++i) {
    pid[i] = fork();
    if (pid[i] != 0)
      continue;
    int m = mutex_open("/mtx", MTX_OPEN | MTX_CREATE, 0777);
    mutextest_op(m, p);
  }
  mutextest_cmp("named", p, pid);
  pipe(p);
  tdata.pipe_end = p[1];
  tdata.mutex = mutex_anon();
  tdata.current_value = 0;
  int pid1 = clone(thread_function, malloc(4096), CLONE_FILES, p);
  int pid2 = clone(thread_function, malloc(4096), CLONE_FILES, p);
  int prev = 0;
  int cur;
  close(p[1]);
  while (read(p[0], &cur, sizeof cur)) {
    if (cur != prev + 1) {
      printf(2, "mutex test 'thread' error!\n");
      kill(pid1);
      kill(pid2);
      exit();
    }
    prev = cur;
  }
  wait();
  wait();
  printf(2, "mutex test 'thread' passed!\n");
}
