#include "user.h"

struct thr {
  int mutex;
  int cur_val;
};

void thread_func(void *arg_) {
  struct thr *p = arg_;
  int pid = getpid();
  for (int i = 0; i < 1000; ++i) {
    mutex_lock(p->mutex);
    int val = p->cur_val++;
    mutex_unlock(p->mutex);
    printf(1, "%d: %d\n", pid, val);
  }
}

int main() {
  struct thr t;
  t.mutex = mutex_anon();
  t.cur_val = 0;
  clone(thread_func, malloc(4096), 0, &t);
  clone(thread_func, malloc(4096), 0, &t);
  wait();
  wait();
  exit();
}
