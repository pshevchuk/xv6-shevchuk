#include "types.h"
#include "defs.h"
#include "mutex.h"
#include "proc.h"
#include "param.h"

int sys_mutex_anon(void) {
  struct mutex *mtx = mutexalloc();
  if (mtx == 0)
    return -1;
  int result = mdalloc(mtx);
  if (result == -1) {
    mutexclose(mtx);
    return -1;
  }
  return result;
}

static int argmd(int n, int *pmd, struct mutex **pm) {
  int md;
  if (argint(n, &md) < 0)
    return -1;
  if (md < 0 || md >= NOMUTEX || proc->mtbl->omutex[md] == 0)
    return -1;
  if (pmd)
    *pmd = md;
  if (pm)
    *pm = proc->mtbl->omutex[md];
  return 0;    
}

int sys_mutex_lock() {
  struct mutex *mtx;
  if (argmd(0, 0, &mtx) < 0)
    return -1;
  
  return mutexlock(mtx);
}

int sys_mutex_unlock() {
  struct mutex *mtx;
  if (argmd(0, 0, &mtx) < 0)
    return -1;
  return mutexunlock(mtx);
}

int sys_mutex_try_lock() {
  struct mutex *mtx;
  if (argmd(0, 0, &mtx) < 0)
    return -1;
  return mutextrylock(mtx);
}

int sys_mutex_close() {
  struct mutex *mtx;
  int md;
  if (argmd(0, &md, &mtx) < 0)
    return -1;
  mutexunlock(mtx);
  int res = mutexclose(mtx);
  if (res == -1)
    return -1;
  proc->mtbl->omutex[md] = 0;
  return res;
}
